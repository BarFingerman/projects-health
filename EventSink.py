import json
from enum import Enum
import pymysql
import FileManager
from Helpers import Configuration


class EventSink():

    projectName = ''

    @staticmethod
    class query(Enum):

        PROBE_IMAGE_CAPTURED = open(FileManager.buildPath(withArgs=['sqlQueries', 'PROBE_IMAGE_CAPTURED']), 'r').read()
        PROBE_MEDIAN_UPLOAD_TIME = open(FileManager.buildPath(withArgs=['sqlQueries', 'PROBE_MEDIAN_UPLOAD_TIME']), 'r').read()
        SESSION_STARTED = open(FileManager.buildPath(withArgs=['sqlQueries', 'SESSION_STARTED']), 'r').read()
        SCENE_STARTED = open(FileManager.buildPath(withArgs=['sqlQueries', 'SCENE_STARTED']), 'r').read()
        SCENE_MEDIAN_UPLOAD_TIME = open(FileManager.buildPath(withArgs=['sqlQueries', 'SCENE_MEDIAN_UPLOAD_TIME']), 'r').read()
        SESSION_MEDIAN_UPLOAD_TIME = open(FileManager.buildPath(withArgs=['sqlQueries', 'SESSION_MEDIAN_UPLOAD_TIME']), 'r').read()

    def connection(self):

        config = Configuration()
        return pymysql.connect(host='eventsink-{}-vpc-mysql.cvjdvv0z1rue.us-east-1.rds.amazonaws.com'.format(config.environment),
                               user='traxreadonly',
                               password='2015Trax@DB',
                               db='eventsdb',
                               charset='utf8mb4',
                               cursorclass=pymysql.cursors.DictCursor)
    
    def __init__(self, projectName=''):
        self.projectName = projectName

    def fetch(self, query=query, jsonKey=''):

        items = set()
        eventSink = EventSink(projectName=self.projectName)
        query = query.value.format(self.projectName)
        connection = eventSink.connection()
        try:
            with connection.cursor() as cursor:
                cursor.execute(query)
                result = cursor.fetchall()
                for uid in result:
                    items.add(uid[jsonKey])
        finally:
            connection.close()
        return items
