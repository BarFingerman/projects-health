from DataAnalysis import DataAnalysis
from DataBase import DataBase

class Project():

    name = ''
    analysis = DataAnalysis('', '')

    def __init__(self, name=name, dataBaseRds=''):

        self.name = name
        self.analysis = DataAnalysis(projectName=self.name, dataBase=DataBase(dataBaseRds))

    @staticmethod
    def all():

        projects = []

        # ccru = Project(name='ccru', dataBaseRds='mysql.ccru-prod.us-east-1.trax-cloud.com')
        # projects.append(ccru)

        hbcde = Project(name='hbcde', dataBaseRds='mysql.hbcde-pro.eu-west-1.trax-cloud.com')
        projects.append(hbcde)

        # solarbr = Project(name='solarbr', dataBaseRds='mysql.solarbr-prod.us-west-2.trax-cloud.com')
        # projects.append(solarbr)

        # integ9 = Project(name='integ9', dataBaseRds='mysql.integ9.us-east-1.trax-cloud.com')
        # projects.append(integ9)

        # lavazzasg = Project(name='lavazzasg', dataBaseRds='mysql.lavazzasg-prod.us-east-1.trax-cloud.com')
        # projects.append(lavazzasg)

        # lavazzait = Project(name='lavazzait', dataBaseRds='mysql.env99.us-east-1.trax-cloud.com')
        # projects.append(lavazzait)

        return projects
