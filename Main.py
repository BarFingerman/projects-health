import pprint
from Project import Project


for project in Project.all():
    project.analysis.start()
    pprint.pprint(project.analysis.results(includeDebugInformation=True))
