from enum import Enum
import pymysql
import FileManager


class DataBase():

    rds = ""

    def __init__(self, rds):
        self.rds = rds

    @staticmethod
    class query(Enum):

        PROBE_SUCCESSFUL_UPLOADS = open(FileManager.buildPath(withArgs=['sqlQueries', 'PROBE_SUCCESSFUL_UPLOADS']), 'r').read()
        SESSION_SUCCESSFUL_UPLOADS = open(FileManager.buildPath(withArgs=['sqlQueries', 'SESSION_SUCCESSFUL_UPLOADS']), 'r').read()
        SCENE_SUCCESSFUL_UPLOADS = open(FileManager.buildPath(withArgs=['sqlQueries', 'SCENE_SUCCESSFUL_UPLOADS']), 'r').read()

    def connection(self):

        return pymysql.connect(host=self.rds,
                               user='traxreadonly',
                               password='2015Trax@DB',
                               db='probedata',
                               charset='utf8mb4',
                               cursorclass=pymysql.cursors.DictCursor)

    def fetch(self, query=query, jsonKey=''):

        items = set()
        connection = self.connection()
        try:
            with connection.cursor() as cursor:
                cursor.execute(query.value)
                result = cursor.fetchall()
                for uid in result:
                    items.add(uid[jsonKey])
        finally:
            connection.close()
        return items