from statistics import median
from DataBase import DataBase
from EventSink import EventSink


class DataAnalysis():

    projectName = ''
    projectDB = DataBase('')
    totalSuccessfulUploads = 0
    totalDeclaredUpload = 0
    missingProbesUIDs = ()
    missingScenesUIDs = ()
    missingSessionsUIDs = ()

    probeKey = 'probes'
    sceneKey = 'scenes'
    sessionKey = 'sessions'

    statistics = {
        probeKey: {},
        sceneKey: {},
        sessionKey: {}
    }

    def __init__(self, projectName=projectName, dataBase=DataBase('')):
        self.projectName = projectName
        self.projectDB = dataBase

    def start(self):

        print('\nanalysis started for {}:'.format(self.projectName))
        self.probe()
        self.session()
        self.scene()

    def probe(self):

        eventSink = EventSink(projectName=self.projectName)
        capturedProbes = eventSink.fetch(query=eventSink.query.PROBE_IMAGE_CAPTURED, jsonKey='image_uid')
        self.statistics[self.probeKey]['captured'] = len(capturedProbes)
        self.totalDeclaredUpload += len(capturedProbes)

        query = DataBase.query.PROBE_SUCCESSFUL_UPLOADS
        uploadedImages = self.projectDB.fetch(query=query, jsonKey='image_uid')
        self.totalSuccessfulUploads += len(uploadedImages)
        self.statistics[self.probeKey]['uploaded'] = len(uploadedImages)

        self.missingProbesUIDs = capturedProbes - uploadedImages

    def probeMedianUploadTime(self):

        uploadsTimeArray = []
        eventSink = EventSink(projectName=self.projectName)
        connection = eventSink.connection()
        query = eventSink.query.PROBE_MEDIAN_UPLOAD_TIME.value.format(self.projectName, self.projectName)
        try:
            with connection.cursor() as cursor:
                cursor.execute(query)
                result = cursor.fetchall()
                for image in result:
                    uploadsTimeArray.append(image['TOTAL_UPLOAD_TIME_IN_SECONDS'])
        finally:
            connection.close()
        if len(uploadsTimeArray) == 0:
            return 0
        return median(uploadsTimeArray)

    def session(self):

        eventSink = EventSink(projectName=self.projectName)
        sessionsStarted = eventSink.fetch(query=eventSink.query.SESSION_STARTED, jsonKey='session_uid')
        self.statistics[self.sessionKey]['started'] = len(sessionsStarted)
        # self.totalDeclaredUpload += len(sessionsStarted)

        query = DataBase.query.SESSION_SUCCESSFUL_UPLOADS
        uploadedSessions = self.projectDB.fetch(query=query, jsonKey='session_uid')
        self.statistics[self.sessionKey]['uploaded'] = len(uploadedSessions)
        # self.totalSuccessfulUploads += len(uploadedSessions)

        self.missingSessionsUIDs = sessionsStarted - uploadedSessions

    def sessionMedianUploadTime(self):

        uploadsTimeArray = []
        eventSink = EventSink(projectName=self.projectName)
        connection = eventSink.connection()
        query = eventSink.query.SESSION_MEDIAN_UPLOAD_TIME.value.format(self.projectName, self.projectName)
        try:
            with connection.cursor() as cursor:
                cursor.execute(query)
                result = cursor.fetchall()
                for image in result:
                    uploadsTimeArray.append(image['TOTAL_UPLOAD_TIME_IN_SECONDS'])
        finally:
            connection.close()
        if len(uploadsTimeArray) == 0:
            return 0
        return median(uploadsTimeArray)

    def scene(self):

        eventSink = EventSink(projectName=self.projectName)
        capturedScenes = eventSink.fetch(query=eventSink.query.SCENE_STARTED, jsonKey='scene_uid')
        self.statistics[self.sceneKey]['started'] = len(capturedScenes)
        self.totalDeclaredUpload += len(capturedScenes)

        query = DataBase.query.SCENE_SUCCESSFUL_UPLOADS
        uploadedScenes = self.projectDB.fetch(query=query, jsonKey='scene_uid')
        self.statistics[self.sceneKey]['uploaded'] = len(uploadedScenes)
        self.totalSuccessfulUploads += len(uploadedScenes)

        self.missingScenesUIDs = capturedScenes - uploadedScenes

    def sceneMedianUploadTime(self):

        uploadsTimeArray = []
        eventSink = EventSink(projectName=self.projectName)
        connection = eventSink.connection()
        query = eventSink.query.SCENE_MEDIAN_UPLOAD_TIME.value.format(self.projectName, self.projectName)
        try:
            with connection.cursor() as cursor:
                cursor.execute(query)
                result = cursor.fetchall()
                for image in result:
                    uploadsTimeArray.append(image['TOTAL_UPLOAD_TIME_IN_SECONDS'])
        finally:
            connection.close()
        if len(uploadsTimeArray) == 0:
            return 0
        return median(uploadsTimeArray)

    def results(self, includeDebugInformation=False):

        self.statistics['totalDeclaredUpload'] = self.totalDeclaredUpload
        self.statistics['totalSuccessfulUploads'] = self.totalSuccessfulUploads

        health = '0%'
        if self.totalDeclaredUpload > 0:
            health = '{}%'.format(int((self.totalSuccessfulUploads / self.totalDeclaredUpload) * 100))
        dictionery = {
                'projectName': self.projectName,
                     'health': health,
        }
        if includeDebugInformation:
            # dictionery['medianUploadTime'] = {
            #                 'images': '{0:.2f} min'.format(self.probeMedianUploadTime()/60),
            #                 'scenes': '{0:.2f} min'.format(self.sceneMedianUploadTime()/60),
            #               'sessions': '{0:.2f} min'.format(self.sessionMedianUploadTime()/60)
            # }
            dictionery["missingItems"] = {
                                'images': len(self.missingProbesUIDs),
                                'scenes': len(self.missingScenesUIDs),
                              'sessions': len(self.missingSessionsUIDs)
            }
            dictionery['statistics'] = self.statistics
        return dictionery
