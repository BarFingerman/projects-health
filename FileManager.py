import os


def buildPath(withArgs=[]):

    path = os.path.dirname(__file__)
    for subPath in withArgs:
        path += '/{}'.format(subPath)
    return path